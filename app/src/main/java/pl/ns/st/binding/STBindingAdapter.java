package pl.ns.st.binding;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.InverseBindingAdapter;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;
import android.widget.TextView;

import pl.ns.st.utils.STHelper;
import pl.ns.st.view.STProgressDialogManager;
import pl.ns.st.view.STToastHelper;

/**
 * Created by n.szydlowski on 09/01/2018.
 */

public class STBindingAdapter {

    @BindingAdapter("android:text")
    public static void setText(TextView view, Long value) {
        boolean setValue = view.getText().length() == 0;
        try {
            if (!setValue) {
                setValue = Long.parseLong(view.getText().toString()) != value;
            }
        } catch (NumberFormatException e) {
        }
        if (setValue) {
            view.setText(String.valueOf(value));
        }
    }

    @InverseBindingAdapter(attribute = "android:text")
    public static Long getText(TextView view) {
        try {
            return Long.parseLong(view.getText().toString());
        } catch (NumberFormatException e) {
            return 0l;
        }
    }

    @BindingAdapter("progress_show")
    public static void showProgressIndeterminate(ViewGroup viewGroup, Boolean show) {

        if (show == null) {
            return;
        }

        if (show) {

            Context context = viewGroup.getContext();

            if (context instanceof AppCompatActivity) {
                STHelper.hideKeyboard((AppCompatActivity)context);
            }

            STHelper.clearFocus(context);

            STProgressDialogManager.get().show(context);
        } else {
            STProgressDialogManager.get().dismiss();
        }
    }

    @BindingAdapter("toast_show")
    public static void showErrorDialog(ViewGroup viewGroup, String message) {

        if (message == null) {
            return;
        }

        Context context = viewGroup.getContext();
        STToastHelper.get().show(context, message);
    }

}
