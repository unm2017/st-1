package pl.ns.st.utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * Created by n.szydlowski on 10/01/2018.
 */

public class STHelper {

    public static void clearFocus(Context context) {

        if(context != null && context instanceof Activity) {

            Activity activity = (Activity) context;

            View focused = activity.getCurrentFocus();
            if(focused != null && focused instanceof EditText) {
                focused.clearFocus();
            }
        }
    }

    public static void hideKeyboard(Activity activity){
        if(activity==null)
            return;

        View view = activity.getCurrentFocus();
        if(view != null){
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}
