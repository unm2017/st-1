package pl.ns.st.view;

import android.content.Context;
import android.support.v4.app.Fragment;

import pl.ns.st.view.interfaces.ISTActivityProvider;
import pl.ns.st.viewmodel.STBaseViewModel;

/**
 * Created by n.szydlowski on 10/01/2018.
 */

public class STBaseFragment extends Fragment {

    private ISTActivityProvider provider;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof ISTActivityProvider) {
            this.provider = (ISTActivityProvider) context;
        }
    }

    protected void attachProvider(STBaseViewModel viewModel) {
        viewModel.setActivityProvider(this.provider);
    }

}
