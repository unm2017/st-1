package pl.ns.st.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pl.ns.st.databinding.FragmentDebtorsBinding;
import pl.ns.st.view.interfaces.ISTActivityProvider;
import pl.ns.st.viewmodel.STDebtorsListViewModel;

/**
 * Created by n.szydlowski on 09/01/2018.
 */

public class STDebtorFragment extends STBaseFragment {

    public final static String TAG = "STDebtorFragment.TAG";

    public static STDebtorFragment newInstance() {
        return new STDebtorFragment();
    }

    private STDebtorsListViewModel viewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.viewModel = ViewModelProviders.of(this).get(STDebtorsListViewModel.class);
        attachProvider(this.viewModel);
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentDebtorsBinding binding = FragmentDebtorsBinding.inflate(inflater, container, false);
        binding.setViewModel(this.viewModel);

        return binding.getRoot();
    }
}
