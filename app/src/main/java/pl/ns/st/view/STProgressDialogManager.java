package pl.ns.st.view;

import android.content.Context;
import android.support.v7.app.AlertDialog;

import pl.ns.st.R;

/**
 * Created by n.szydlowski on 10/01/2018.
 */

public class STProgressDialogManager {

    private static final STProgressDialogManager INSTANCE = new STProgressDialogManager();

    public static STProgressDialogManager get() {
        return INSTANCE;
    }

    private AlertDialog dialog;

    public void show(Context context) {
        dismiss();

        if (context == null) return;

        dialog = new AlertDialog.Builder(context, R.style.ProgressDialogTheme)
                .setView(R.layout.dialog_progress)
                .setCancelable(false)
                .show();
    }

    public void dismiss() {
        if (dialog != null && this.dialog.isShowing()) dialog.dismiss();

        dialog = null;
    }


}
