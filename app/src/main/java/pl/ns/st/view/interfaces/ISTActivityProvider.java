package pl.ns.st.view.interfaces;

import android.support.v4.app.Fragment;

/**
 * Created by n.szydlowski on 10/01/2018.
 */

public interface ISTActivityProvider {

    void attachFragment(Fragment fragment, String tag, boolean backstack);
    String getString(int res_id);

}
