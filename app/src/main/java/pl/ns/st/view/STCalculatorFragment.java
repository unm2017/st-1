package pl.ns.st.view;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pl.ns.st.databinding.FragmentCalculatorBinding;
import pl.ns.st.model.ISTDebtor;
import pl.ns.st.viewmodel.STCalculatorViewModel;
import pl.ns.st.viewmodel.STLoanListViewModel;


/**
 * Created by n.szydlowski on 09/01/2018.
 */

public class STCalculatorFragment extends STBaseFragment {

    public static final String TAG = "STCalculatorFragment.TAG";

    private static final String KET_ID = "STCalculatorFragment.ID";

    public static STCalculatorFragment newInstance(ISTDebtor debtor) {
        Bundle args = new Bundle(1);
        args.putInt(KET_ID, debtor.getIdentifier());
        STCalculatorFragment fragment = new STCalculatorFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private STCalculatorViewModel viewModel;
    private STLoanListViewModel viewModelLoan;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.viewModel = ViewModelProviders.of(this).get(STCalculatorViewModel.class);
        this.viewModelLoan = ViewModelProviders.of(this).get(STLoanListViewModel.class);
        this.viewModel.setOnUpdateLoans(this.viewModelLoan);

        if(getArguments() != null) {
            int debtorId = getArguments().getInt(KET_ID, - 1);
            this.viewModelLoan.setDebtorId(debtorId);
        }

        attachProvider(this.viewModel);
        attachProvider(this.viewModelLoan);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentCalculatorBinding binding = FragmentCalculatorBinding.inflate(inflater, container, false);
        binding.setViewModel(this.viewModel);
        binding.setViewModelLoan(this.viewModelLoan);

        return binding.getRoot();
    }

}
