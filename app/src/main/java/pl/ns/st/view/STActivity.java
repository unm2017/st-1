package pl.ns.st.view;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import pl.ns.st.R;
import pl.ns.st.view.interfaces.ISTActivityProvider;

public class STActivity extends AppCompatActivity implements ISTActivityProvider {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_st);

        attachFragment(STDebtorFragment.newInstance(), STDebtorFragment.TAG,  true);
    }

    public void attachFragment(Fragment fragment, String tag, boolean backstack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_fragment_id, fragment, tag);

        if (backstack) {
            fragmentTransaction.addToBackStack(null);
        }

        fragmentTransaction.commit();
    }
}
