package pl.ns.st.view;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by n.szydlowski on 10/01/2018.
 */

public class STToastHelper {

    private static final STToastHelper INSTANCE = new STToastHelper();

    public static STToastHelper get() {
        return INSTANCE;
    }

    public void show(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

}
