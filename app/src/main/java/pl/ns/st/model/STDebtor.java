package pl.ns.st.model;

/**
 * Created by n.szydlowski on 09/01/2018.
 */


/**
 * Pola zgodnie z trescia zadania
 */
public class STDebtor implements ISTDebtor {

    private int id;
    private String imie_nazwisko;
    private Long dlug;

    @Override
    public int getIdentifier() {
        return this.id;
    }

    @Override
    public String getFullName() {
        return this.imie_nazwisko;
    }

    @Override
    public Long getDebit() {
        return this.dlug;
    }
}
