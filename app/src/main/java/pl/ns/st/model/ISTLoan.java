package pl.ns.st.model;


/**
 * Created by n.szydlowski on 09/01/2018.
 */

public interface ISTLoan {

    int getOrdinal();
    Long getValue();
    String getDate();
}
