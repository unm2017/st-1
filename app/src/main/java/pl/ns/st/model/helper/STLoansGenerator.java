package pl.ns.st.model.helper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import pl.ns.st.model.ISTLoan;
import pl.ns.st.model.STLoan;

/**
 * Created by n.szydlowski on 09/01/2018.
 */

public class STLoansGenerator {

    private Long lastValue;
    private Long lastLoanNumbers;

    private ArrayList<ISTLoan> lastGenerateLoans;
    private DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

    public ArrayList<ISTLoan> generate(Long value, Long loansNumber) {
        if (this.lastValue != null && this.lastLoanNumbers != null && this.lastValue == value && this.lastLoanNumbers == loansNumber) {
            return this.lastGenerateLoans;
        }

        this.lastValue = value;
        this.lastLoanNumbers = loansNumber;

        generateNewLoans();
        return this.lastGenerateLoans;
    }

    public ArrayList<ISTLoan> getLastGenerateLoans() {
        return this.lastGenerateLoans;
    }

    private void generateNewLoans() {

        Long smallValue = this.lastValue / this.lastLoanNumbers;
        Long restValue  = this.lastValue - (smallValue * this.lastLoanNumbers);

        ArrayList<ISTLoan> loans = new ArrayList<>();

        Date today = new Date();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);



        for (int i = 0; i < this.lastLoanNumbers; i++) {

            Long value = i == this.lastLoanNumbers - 1 ? smallValue + restValue : smallValue;

            calendar.add(Calendar.MONTH, 1);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.add(Calendar.DATE, -1);

            STLoan loan = new STLoan(i + 1, value, formatter.format(calendar.getTime()));
            loans.add(loan);

            calendar.add(Calendar.MONTH, 1);
        }

        this.lastGenerateLoans = loans;
    }

}
