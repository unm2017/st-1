package pl.ns.st.model;


/**
 * Created by n.szydlowski on 09/01/2018.
 */

public class STLoan implements ISTLoan {

    private int lp;
    private Long kwota;
    private String data;

    public STLoan(int ordinal, Long value, String date) {
        this.lp = ordinal;
        this.kwota = value;
        this.data = date;
    }

    @Override
    public int getOrdinal() {
        return this.lp;
    }

    @Override
    public Long getValue() {
        return this.kwota;
    }

    @Override
    public String getDate() {
        return this.data;
    }
}
