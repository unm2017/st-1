package pl.ns.st.model.helper;

import java.util.Comparator;

import pl.ns.st.model.ISTDebtor;

/**
 * Created by n.szydlowski on 09/01/2018.
 */

public class STHelperModel {

    /**
     * mozna rowniez uzyc biblioteki do streamow
     */
    public static Comparator<ISTDebtor> debtorComparator = new Comparator<ISTDebtor>() {

        public int compare(ISTDebtor debtor1, ISTDebtor debtor2) {
            return Integer.compare(debtor1.getIdentifier(), debtor2.getIdentifier());
        }

    };

}
