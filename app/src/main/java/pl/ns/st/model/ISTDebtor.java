package pl.ns.st.model;

/**
 * Created by n.szydlowski on 09/01/2018.
 */

public interface ISTDebtor {
    int getIdentifier();
    String getFullName();
    Long getDebit();
}
