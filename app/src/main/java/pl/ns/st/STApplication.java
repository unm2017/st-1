package pl.ns.st;

import android.app.Application;

import pl.ns.st.api.STConnection;
import pl.ns.st.provider.STAppDataProvider;

/**
 * Created by n.szydlowski on 09/01/2018.
 */

public class STApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        STConnection.init();
        STAppDataProvider.init();
    }

}
