package pl.ns.st.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableField;

import pl.ns.st.view.interfaces.ISTActivityProvider;

/**
 * Created by n.szydlowski on 09/01/2018.
 */

public class STBaseViewModel extends ViewModel {

    public final ObservableField<Boolean> show = new ObservableField<>(Boolean.FALSE);
    public final ObservableField<String> toastMessage = new ObservableField<>();

    protected ISTActivityProvider activityProvider;

    public void show() {
        this.show.set(Boolean.TRUE);
    }

    public void hide() {
        this.show.set(Boolean.FALSE);
    }

    protected void setToastMessage(String message) {
        this.toastMessage.set(null);
        this.toastMessage.set(message);
    }

    public void setActivityProvider(ISTActivityProvider activityProvider) {
        this.activityProvider = activityProvider;
    }

}
