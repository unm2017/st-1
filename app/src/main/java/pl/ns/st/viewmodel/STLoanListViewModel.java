package pl.ns.st.viewmodel;

import android.databinding.ObservableField;

import java.util.ArrayList;

import pl.ns.st.R;
import pl.ns.st.adapter.STBaseRecyclerViewAdapter;
import pl.ns.st.model.ISTLoan;
import pl.ns.st.model.helper.STLoansGenerator;
import pl.ns.st.provider.ISTDataProvider;
import pl.ns.st.provider.STAppDataProvider;

/**
 * Created by n.szydlowski on 09/01/2018.
 */

public class STLoanListViewModel extends STBaseViewModel implements STOnUpdateLoans, ISTDataProvider<Boolean>{


    private LoanRecyclerViewAdapter loansRecyclerViewAdapter = new LoanRecyclerViewAdapter();
    public final ObservableField<LoanRecyclerViewAdapter> adapter = new ObservableField<>(this.loansRecyclerViewAdapter);

    public final ObservableField<Boolean> isButtonEnabled = new ObservableField<>(Boolean.TRUE);
    private STLoansGenerator generator = new STLoansGenerator();

    private int debtorId = -1;

    public void clickSendLoans() {
        show();
        STAppDataProvider.init().sendLoansList(this, this.generator.getLastGenerateLoans(), this.debtorId);
    }

    public void setDebtorId(int debtorId) {
        this.debtorId = debtorId;
    }

    @Override
    public void onReceiveValues(Long value, Long loansNumber) {
        this.isButtonEnabled.set(Boolean.TRUE);
        ArrayList<ISTLoan> loans = this.generator.generate(value, loansNumber);
        this.loansRecyclerViewAdapter.setItems(loans);
    }

    @Override
    public void onNotValidValues() {
        this.isButtonEnabled.set(Boolean.FALSE);
        this.loansRecyclerViewAdapter.setItems(null);
    }

    @Override
    public void onLoadData(Boolean item) {
        hide();

        if(this.activityProvider != null) {
            setToastMessage(this.activityProvider.getString(R.string.calcualtor_send_success));
        }
    }

    @Override
    public void onLoadFailed(String message) {
        hide();
        setToastMessage(message);
    }

    public class LoanRecyclerViewAdapter extends STBaseRecyclerViewAdapter<ISTLoan> {


        @Override
        protected int getLayoutIdForPosition(int position) {
            return R.layout.row_loan;
        }
    }

}
