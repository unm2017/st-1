package pl.ns.st.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableField;
import java.util.Collections;
import java.util.List;

import pl.ns.st.R;
import pl.ns.st.adapter.STBaseRecyclerViewAdapter;
import pl.ns.st.adapter.STItemClickListener;
import pl.ns.st.model.ISTDebtor;
import pl.ns.st.model.helper.STHelperModel;
import pl.ns.st.provider.ISTDataProvider;
import pl.ns.st.provider.STAppDataProvider;
import pl.ns.st.view.STCalculatorFragment;

/**
 * Created by n.szydlowski on 09/01/2018.
 */

public class STDebtorsListViewModel extends STBaseViewModel implements ISTDataProvider<List<ISTDebtor>>, STItemClickListener<ISTDebtor> {

    private DebtorsRecyclerViewAdapter debtorsRecyclerViewAdapter = new DebtorsRecyclerViewAdapter();
    public final ObservableField<DebtorsRecyclerViewAdapter> adapter = new ObservableField<>(this.debtorsRecyclerViewAdapter);
    public final ObservableField<Boolean> isLoading = new ObservableField<>(Boolean.FALSE);

    public STDebtorsListViewModel() {
        this.debtorsRecyclerViewAdapter.setItemClickListener(this);
        dowloadData();
    }

    @Override
    public void onLoadData(List<ISTDebtor> item) {
        hide();
        this.isLoading.set(Boolean.FALSE);
        this.debtorsRecyclerViewAdapter.setItems(item);
    }

    @Override
    public void onLoadFailed(String message) {
        hide();
        this.isLoading.set(Boolean.FALSE);
        setToastMessage(message);
    }

    public void onRefresh() {
        this.isLoading.set(Boolean.TRUE);
        dowloadData();
    }

    private void dowloadData() {
        if (this.debtorsRecyclerViewAdapter.getItemCount() == 0) {
            show();
        }
        STAppDataProvider.init().getDebtorsList(this);
    }

    @Override
    public void onItemClick(ISTDebtor item) {
        if(this.activityProvider != null) {
            this.activityProvider.attachFragment(STCalculatorFragment.newInstance(item), STCalculatorFragment.TAG, true);
        }
    }

    public class DebtorsRecyclerViewAdapter extends STBaseRecyclerViewAdapter<ISTDebtor> {

        @Override
        public void setItems(List<ISTDebtor> items) {
            Collections.sort(items, STHelperModel.debtorComparator);
            super.setItems(items);
        }

        @Override
        protected int getLayoutIdForPosition(int position) {
            return R.layout.row_debtor;
        }
    }

}
