package pl.ns.st.viewmodel;

/**
 * Created by n.szydlowski on 09/01/2018.
 */

public interface STOnUpdateLoans {

    void onReceiveValues(Long value, Long loansNumber);
    void onNotValidValues();
}
