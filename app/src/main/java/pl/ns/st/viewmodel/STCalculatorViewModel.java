package pl.ns.st.viewmodel;

import android.databinding.ObservableField;
import android.util.Log;
import android.widget.SeekBar;

/**
 * Created by n.szydlowski on 09/01/2018.
 */

public class STCalculatorViewModel extends STBaseViewModel {

    private final Long minLoanValue = 50000l; //grosze ??

    private Double minLoanNumber = 3.0;
    private Double maxLoanNumber = 6.0;

    public final ObservableField<Long> loanNumber = new ObservableField<>(3l);
    public final ObservableField<Long> loanValue = new ObservableField<>(50000l);
    public final ObservableField<Long> seekBarValue = new ObservableField<>(0l);

    private STOnUpdateLoans onUpdateLoans;

    public void onValueChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
        if (fromUser) {

            Double value = ((this.maxLoanNumber - this.minLoanNumber) * ( (double) progresValue / 100.0) + this.minLoanNumber);

            this.loanNumber.set(value.longValue());
        }
    }

    public void onTextChangedValue(CharSequence s, int start, int before, int count) {

        Double value = s.length() == 0 ? 0.0 : Double.valueOf(s.toString());

        if (value < this.minLoanValue ) {
            updateNotValidCalculatorData();
            return;
        }

        this.loanValue.set(value.longValue());
        changeLaonsMaxNumber(value);
    }

    public void onTextChangedNumberLoans(CharSequence s, int start, int before, int count) {

        Double value = s.length() == 0 ? 0.0 : Double.valueOf(s.toString());

        if (value < this.minLoanNumber || value > this.maxLoanNumber) {
            updateNotValidCalculatorData();
            return;
        }

        this.loanNumber.set(value.longValue());
        recalculateSeekBarIfNeed();
    }

    public void setOnUpdateLoans(STOnUpdateLoans onUpdateLoans) {
        this.onUpdateLoans = onUpdateLoans;
    }

    private void changeLaonsMaxNumber(Double value) {
        if (value <= 1000) {
            this.maxLoanNumber = 6.0;
        } else if (value >= 2000) {
            this.maxLoanNumber = 24.0;
        } else {
            this.maxLoanNumber = 12.0;
        }

        recalculateLoansValueIfNeed();
        recalculateSeekBarIfNeed();
    }

    private void recalculateLoansValueIfNeed() {

        if (this.loanNumber.get() <= this.maxLoanNumber) {
            return;
        }

        this.loanNumber.set(this.maxLoanNumber.longValue());
    }

    private void recalculateSeekBarIfNeed() {
        Double progress = 100 * (this.loanNumber.get() - this.minLoanNumber) / (this.maxLoanNumber - this.minLoanNumber);
        this.seekBarValue.set(progress.longValue());
        updateCalculatorData();
    }

    private void updateCalculatorData() {

        if (this.onUpdateLoans != null) {
            this.onUpdateLoans.onReceiveValues(this.loanValue.get(), this.loanNumber.get());
        }

    }

    private void updateNotValidCalculatorData() {
        if(this.onUpdateLoans != null) {
            this.onUpdateLoans.onNotValidValues();
        }
    }

}
