package pl.ns.st.api;


import java.util.List;

import pl.ns.st.model.ISTLoan;

/**
 * Created by n.szydlowski on 09/01/2018.
 */

public class STDebtorLoans {

    private int id;
    private List<ISTLoan> loans;

    public STDebtorLoans(int id, List<ISTLoan> loans) {
        this.id = id;
        this.loans = loans;
    }
}
