package pl.ns.st.api;

import java.util.List;

import pl.ns.st.model.STDebtor;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by n.szydlowski on 09/01/2018.
 */

public interface STService {

    @GET("api/debtors")
    Call<List<STDebtor>> getDebtorsList();

    @POST("api/debtor")
    Call<Boolean> postDebtorLoans(@Body STDebtorLoans debtorLoans);

}
