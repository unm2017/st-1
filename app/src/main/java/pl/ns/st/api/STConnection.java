package pl.ns.st.api;

import java.util.List;

import pl.ns.st.model.STDebtor;
import retrofit2.Call;

/**
 * Created by n.szydlowski on 09/01/2018.
 */

public class STConnection {

    private static STConnection INSTANCE;

    public static STConnection init(){
        if(INSTANCE == null){
            synchronized(STConnection.class){
                if(INSTANCE == null)
                    INSTANCE = new STConnection();
            }
        }
        return INSTANCE;
    }

    private STClient stClient;

    private STConnection() {
        stClient = new STClient();
    }

    public static STConnection get() {
        return INSTANCE;
    }


    public Call<List<STDebtor>> getDebtorsList() {
        STService service = this.stClient.getService();
        return service.getDebtorsList();
    }

    public Call<Boolean> postDebtorLoans(STDebtorLoans debtorLoans) {
        STService service = this.stClient.getService();
        return service.postDebtorLoans(debtorLoans);
    }
}
