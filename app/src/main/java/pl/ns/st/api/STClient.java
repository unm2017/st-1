package pl.ns.st.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import pl.ns.st.BuildConfig;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by n.szydlowski on 09/01/2018.
 */

public class STClient {

    private static final String API_URL = "http://demo9891015.mockable.io";

    private static final String DATE_FORMAT = "dd-MM-yyyy";

    private static Gson gson = new GsonBuilder()
            .setDateFormat(DATE_FORMAT)
            .create();

    private STService syncService;

    public STClient(){
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(API_URL);
        builder.addConverterFactory(GsonConverterFactory.create(gson));
        builder.client(getHttpClient());

        Retrofit restAdapter = builder.build();
        this.syncService = restAdapter.create(STService.class);
    }

    private OkHttpClient getHttpClient() {
        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
        okHttpClient.readTimeout(15 * 1000, TimeUnit.MILLISECONDS);
        okHttpClient.connectTimeout(8 * 1000, TimeUnit.MILLISECONDS);

        if (BuildConfig.DEBUG) okHttpClient.addNetworkInterceptor(getLoggingInterceptor());

        return okHttpClient.build();
    }

    private HttpLoggingInterceptor getLoggingInterceptor(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return interceptor;
    }

    public STService getService() {
        return this.syncService;
    }

    public static STConnection get() {
        return STConnection.get();
    }

}
