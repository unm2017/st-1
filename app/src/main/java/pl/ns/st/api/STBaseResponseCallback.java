package pl.ns.st.api;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by n.szydlowski on 09/01/2018.
 */

public abstract class STBaseResponseCallback<T> implements Callback<T> {

    public abstract void onError(String error);

    public abstract void onSuccess(T response);

    @Override
    public void onResponse(Call<T> call, Response<T> response) {


        if (response.isSuccessful()) {
            T responseBody = response.body();
            onSuccess(responseBody);

        } else {

            try {

                ResponseBody responseBody = response.errorBody();

                if (responseBody != null) {
                    onError(responseBody.string());
                } else {
                    onError(null);
                }

            } catch (IOException e){
                onError(e.getLocalizedMessage());
            }
        }

    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        onError(t.getLocalizedMessage());
    }
}

