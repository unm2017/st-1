package pl.ns.st.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import pl.ns.st.BR;

/**
 * Created by n.szydlowski on 09/01/2018.
 */

public abstract class STBaseRecyclerViewAdapter<T> extends RecyclerView.Adapter<STBaseRecyclerViewAdapter.BaseViewHolder<T>> {

    protected abstract int getLayoutIdForPosition(int position);

    protected STItemClickListener<T> itemClickListener;

    protected List<T> items = new ArrayList<>();


    public void setItems(List<T> items) {

        clear();

        if (items != null) {
            this.items.addAll(items);
        }

        notifyDataSetChanged();
    }

    public void clear() {
        this.items.clear();
    }


    public void setItemClickListener(STItemClickListener<T> itemClickListener) {
        this.itemClickListener = itemClickListener;
    }


    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());


        ViewDataBinding binding = DataBindingUtil.inflate(layoutInflater, viewType, parent, false);

        return getViewHolder(binding);
    }

    @Override
    public int getItemViewType(int position) {
        return getLayoutIdForPosition(position);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        T item = getItemForPosition(position);
        holder.bind(item);
        holder.setOnClickListener(this.itemClickListener);
    }

    public BaseViewHolder getViewHolder(ViewDataBinding binding) {
        return new BaseViewHolder(binding);
    }

    protected T getItemForPosition(int position) {
        return this.items.get(position);
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    public static class BaseViewHolder<T> extends RecyclerView.ViewHolder {

        private final ViewDataBinding binding;

        public BaseViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void bind(T item) {
            this.binding.setVariable(BR.item, item);
            this.binding.executePendingBindings();
        }

        public void setOnClickListener(STItemClickListener<T> itemClickListener) {
            this.binding.setVariable(BR.callback, itemClickListener);
        }

        public ViewDataBinding getBinding() {
            return this.binding;
        }

    }

}
