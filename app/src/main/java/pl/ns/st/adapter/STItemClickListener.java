package pl.ns.st.adapter;

/**
 * Created by n.szydlowski on 09/01/2018.
 */

public interface STItemClickListener<T> {

    void onItemClick(T item);
}
