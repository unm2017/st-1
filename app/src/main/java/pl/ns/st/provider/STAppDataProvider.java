package pl.ns.st.provider;


import java.util.ArrayList;
import java.util.List;

import pl.ns.st.api.STBaseResponseCallback;
import pl.ns.st.api.STClient;
import pl.ns.st.api.STConnection;
import pl.ns.st.api.STDebtorLoans;
import pl.ns.st.model.ISTDebtor;
import pl.ns.st.model.ISTLoan;
import pl.ns.st.model.STDebtor;
import retrofit2.Call;

/**
 * Created by n.szydlowski on 09/01/2018.
 */

/**
 * mock class
 */
public class STAppDataProvider {

    private static STAppDataProvider INSTANCE;

    public static STAppDataProvider init(){
        if(INSTANCE == null){
            synchronized(STAppDataProvider.class){
                if(INSTANCE == null)
                    INSTANCE = new STAppDataProvider();
            }
        }
        return INSTANCE;
    }

    private ISTDataProvider<List<ISTDebtor>> debtorsCallback;
    private Call<List<STDebtor>> callDebtors;

    private ISTDataProvider<Boolean> loansCallback;
    private Call<Boolean> callLoans;

    public void getDebtorsList(ISTDataProvider<List<ISTDebtor>> debtorsCallback) {

        this.debtorsCallback = debtorsCallback;
        cancelCall(this.callDebtors);
        this.callDebtors = STClient.get().getDebtorsList();
        this.callDebtors.enqueue(new STBaseResponseCallback<List<STDebtor>>() {
            @Override
            public void onError(String error) {
                STAppDataProvider.this.debtorsCallback.onLoadFailed(error);
            }

            @Override
            public void onSuccess(List<STDebtor> response) {
                STAppDataProvider.this.debtorsCallback.onLoadData(new ArrayList<ISTDebtor>(response));
            }
        });
    }

    public void sendLoansList(ISTDataProvider<Boolean> loansCallback, List<ISTLoan> loans, int debtorID) {
        this.loansCallback = loansCallback;

        STDebtorLoans debtorLoans = new STDebtorLoans(debtorID, loans);

        cancelCall(this.callLoans);
        this.callLoans = STClient.get().postDebtorLoans(debtorLoans);
        this.callLoans.enqueue(new STBaseResponseCallback<Boolean>() {
            @Override
            public void onError(String error) {
                STAppDataProvider.this.loansCallback.onLoadFailed(error);
            }

            @Override
            public void onSuccess(Boolean response) {
                STAppDataProvider.this.loansCallback.onLoadData(response);
            }
        });
    }

    private void cancelCall(Call call) {
        if (call != null && !call.isCanceled()) {
            call.cancel();
        }
    }

    public void cancelLoad() {
        cancelCall(this.callDebtors);
    }

}
