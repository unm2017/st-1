package pl.ns.st.provider;

/**
 * Created by n.szydlowski on 09/01/2018.
 */

public interface ISTDataProvider<T> {

    void onLoadData(T item);
    void onLoadFailed(String message);

}
